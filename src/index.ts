import "./styles/index.css";
import * as $ from "jquery";



const main = () => {
const users = document.getElementById('users')

    $.get('https://jsonplaceholder.typicode.com/users',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if(textStatus === 'success'){
                console.log(data)
                data.forEach((data:any): void=> {
                    users.innerHTML += `<tr>
                            <td class="text-black px-6 py-4 whitespace-nowrap">${data.name}</td>
                            <td class="px-6 py-4 whitespace-nowrap">${data.email}</td>
                            <td class="px-6 py-4 whitespace-nowrap">Utilisateur</td>
                            <td class="px-6 py-4 whitespace-nowrap">14/02/2022 - 09:29</td>
                            <td class="px-6 py-4 whitespace-nowrap"><i class="fas fa-check"></i></td>
                            <td class="px-6 py-4 whitespace-nowrap"><a href="">Modifier</a></td>
                        </tr>`
                })
            }
        });


}


main();
